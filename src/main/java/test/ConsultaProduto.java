package test;

import core.BaseTest;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

public class ConsultaProduto extends BaseTest {



    @Test
    public void consultaProdutosCadastrados () {
        given()
                .when()
                .get("/products")
                .then()
                .log().all()
                .statusCode(200) // Verificar o status code esperado
//                .body("total", is(100))// Verificar total
        ;

    }

    @Test
    public void ConsultaProdutosCadastradosPorID () {
        given()
                .when()
                .get("/products/1")
                .then()
                .log().all()
                .statusCode(200) // Verificar o status code esperado
                .body("title", is("iPhone 9"))// Verificar total
        ;

    }

    @Test
    public void ConsultaProdutosCadastradosPorIDNaoCadastrado () {
        given()
                .when()
                .get("/products/0")
                .then()
                .log().all()
                .statusCode(404) // Verificar o status code esperado
                .body("message", is("Product with id '0' not found"))// Verificar total
        ;

    }






}
