package test;

import core.BaseTest;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class UsuariosCadastrado extends BaseTest {

    @Test
    public void testConsultaUsuariosCadastrados () {
        given()
                .when()
                .get("/users")
                .then()
                .log().all()
                .statusCode(200) // Verificar o status code esperado
                .body("total", is(100))// Verificar total
        ;

    }

    @Test
    public void testUsuarioPorId () {
        given()
                .when()
                .get("/users")
                .then()
                .log().all()
                .statusCode(200) // Verificar o status code esperado
                .body("users[1].username", not(emptyString())) // Verificar se o campo "username" não está vazio no primeiro usuário
                .body("users[1].password", not(emptyString())) // Verificar se o campo "password" não está vazio no primeiro usuário
        ;
    }

    @Test
    public void testVerificarSeOsCamposNaoEstaoVazios() {

        given()
                .when()
                .get("/users")
                .then()
                .log().all() // Para imprimir a resposta no console (opcional)
                .statusCode(200) // Verificar o status code esperado
                .body("users", not(empty())) // Verificar se a matriz "users" não está vazia
                .body("users.username", everyItem(not(emptyString()))) // Verificar se o campo "username" não está vazio em todos os usuários
                .body("users.password", everyItem(not(emptyString()))); // Verificar se o campo "password" não está vazio em todos os usuários
    }
}




