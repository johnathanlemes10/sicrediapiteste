package test;

import core.BaseTest;
import io.restassured.RestAssured;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class AdicionarProdutos extends BaseTest {
    private static Integer PRODUTO_ID;
   @BeforeClass
    public static void login() {
        Map<String, String> login = new HashMap<>();
        login.put("username","kminchelle");
        login.put("password","0lelplR");

        String TOKEN = given()
                .body(login)
                .when()
                .post("/auth/login")
                .then()
                .log().all()
                .statusCode(200)
                .extract().path("token");


        RestAssured.requestSpecification.header("Authorization", "JWT " + TOKEN);

    }

    @Test
    public void T01_deveIncluirProdutoComSucesso() {
        // JSON do produto a ser adicionado
        String produtoJSON = "{\n" +
                "    \"title\": \"Perfume Oil\",\n" +
                "    \"description\": \"Mega Discount, Impression of A...\",\n" +
                "    \"price\": 13,\n" +
                "    \"discountPercentage\": 8.4,\n" +
                "    \"rating\": 4.26,\n" +
                "    \"stock\": 65,\n" +
                "    \"brand\": \"Impression of Acqua Di Gio\",\n" +
                "    \"category\": \"fragrances\",\n" +
                "    \"thumbnail\": \"https://i.dummyjson.com/data/products/11/thumbnail.jpg\"\n" +
                "}";

        // Envia a solicitação POST para adicionar o produto
        PRODUTO_ID = given()
                .body(produtoJSON)
                .when()
                .post("/products/add")
                .then()
                .log().all()
                .statusCode(200) // Verificar o status code esperado
                .extract().path("id")
        ;
    }


}
