package test;

import core.BaseTest;
import io.qameta.allure.junit4.DisplayName;
import org.junit.Test;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class StatusAPI extends BaseTest {

    @Test
    @DisplayName("Human-readable test name")
    public void testAPIStatus () {
        given()
                .when()
                .get("/test")
                .then()
                .log().all()
                .statusCode(200) // Verificar o status code esperado
                .body("status", equalTo("ok")) // Verificar o campo "status" no corpo da resposta
                .body("method", equalTo("GET"))// Verificar o campo "method" no corpo da resposta
                ;

    }
}

